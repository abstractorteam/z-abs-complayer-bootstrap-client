
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import GuidGenerator from 'z-abs-corelayer-cs/guid-generator';
import React from 'react';


class Button extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.id = this.props.id ? this.props.id : GuidGenerator.create();
    this.$popover = null;
    this.clearId = 0;
  }
  
  shouldUpdate(nextProps, nextState) {
    return this.props !== nextProps;
  }
  
  didMount() {
    const self = this;
    const shortcut = this.props.shortcut ? `<span class="bootstrap_popover"> (${this.props.shortcut})</span>` : '';
    this.$popover = $(`#${this.id}`).popover({html:true,container:'body',delay:{show:500,hide:0},trigger:'hover',content:function() {
      return '<div class="bootstrap_popover"><img class="bootstrap_popover" src="'+$(this).data('img') + `" /><p class="bootstrap_popover">${self._getContent()}${shortcut}</p></div>`;
    }});
    this.$popover.on('shown.bs.popover', function () {
      self.clearId = setTimeout(() => {
        self.$popover.popover('hide');
      }, 3000);
    });
    this.$popover.on('hidden.bs.popover', function () {
      if(0 !== self.clearId) {
        clearTimeout(self.clearId);
        self.clearId = 0;
      }
    });
  }
  
  didUpdate() {
    const popover = this.$popover.data('bs.popover');
    if(null !== popover.tip()[0].lastChild.firstChild) {
      popover.tip()[0].lastChild.firstChild.lastChild.innerHTML = this._getContent();
    }
  }
  
  willUnmount() {
    this.$popover.popover('destroy');
  }
  
  render() {
    const placement = this.props.placement ? this.props.placement : 'bottom';
    const title = this.props.title ? this.props.title : '';
    const disabled = this.props.disabled ? ' disabled' : '';
    const active = this.props.active ? ' active' : '';
    const type = this.props.type ? ` btn-${this.props.type}` : ' btn-default';
    const className = this.props.className ? ` ${this.props.className}` : '';
    return (
      <button id={this.id} type="button" className={`btn ${type}${disabled}${active}${className}`} data-toggle="popover" title={title} data-placement={placement} data-img="/images/svg/AbstraktorA.svg" style={this.props.style}
        onClick={(e) => {
          if(!this.props.disabled) {
            this.props.onClick && this.props.onClick(e);
          }
        }}>
        {this.props.children}
      </button>
    );
  }
  
  _getContent() {
    let content = '';
    if(typeof this.props.content === 'string') {
      content = this.props.content;
    }
    else if(typeof this.props.content === 'function') {
      content = this.props.content();
    }
    return this.props.heading ? `<strong>${this.props.heading}:</strong> ${content}` : content; 
  }
}


module.exports = Button;
