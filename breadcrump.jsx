
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import Link from 'z-abs-complayer-router-client/project/client/react-component/link';
import React from 'react';


export default class ComponentBreadcrump extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.deepCompare(this.props, nextProps);
  }
  
  renderBreadcrumbSlash(index) {
    if(0 !== index) {
      return (
        <div className="bootstrap_breadcrump_item">
          /
        </div>
      );
    }
  }
  
  renderBreadcrumbItem(name, link, index) {
    if(undefined !== name) {
      return (
        <div key={index}>
          {this.renderBreadcrumbSlash(index)}
          <Link className="bootstrap_breadcrump_item" href={link} replace>
            <div className="bootstrap_breadcrump_item">
              {name}
            </div>
          </Link>
        </div>
      );
    }
  }
  
  renderBreadcrumb() {
    return this.props.items.map((item, index) => {
      return this.renderBreadcrumbItem(item.name, item.link, index);
    });
  }
  
  render() {
    return (
      <div className="bootstrap_breadcrump">
        {this.renderBreadcrumb()}
      </div>
    );
  }
}
