
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import GuidGenerator from 'z-abs-corelayer-cs/guid-generator';
import React from 'react';


export class Image extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.guid = GuidGenerator.create();
    this.$popover = null;
    this.clearId = 0;
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props, nextProps);
  }
  
  didMount() {
    const self = this;
    let content = '';
    if(typeof this.props.content === 'string') {
      content = this.props.content;
    }
    else if(typeof this.props.content === 'function') {
      content = this.props.content();
    }
    let headContent = this.props.heading ? `<strong>${this.props.heading}:</strong> ${content}` : content;
    this.$popover = $(`#${this.guid}`).popover({html:true,container:'body',trigger:'hover',delay:{show:1000,hide:0},content:function() {
      return '<div class="bootstrap_popover"><img class="bootstrap_popover" src="'+$(this).data('img') + `" /><p class="bootstrap_popover">${headContent}</p></div>`;
    }});
    this.$popover.on('shown.bs.popover', function () {
      self.clearId = setTimeout(() => {
        self.$popover.popover('hide');
      }, 3000);
    });
    this.$popover.on('hidden.bs.popover', function () {
      if(0 !== self.clearId) {
        clearTimeout(self.clearId);
        self.clearId = 0;
      }
    });
  }
  
  willUnmount() {
    this.$popover.popover('destroy');
  }
  
  render() {
    const placement = this.props.placement ? this.props.placement : 'bottom';
    const title = this.props.title ? this.props.title : '';
    const disabled = this.props.disabled ? ' disabled' : '';
    const active = this.props.active ? ' active' : '';
    const className = this.props.className ? ` ${this.props.className}` : '';
    return (
      <img id={this.guid} tabIndex="50" src={this.props.src} className={`${className}${disabled}${active}`} style={this.props.style} data-toggle="popover" title={title} data-placement={placement} data-img="/images/svg/AbstraktorA.svg"
        onClick={(e) => {
          if(!this.props.disabled) {
            this.props.onClick && this.props.onClick(e);
          }
        }}>
        {this.props.children}
      </img>
    );
  }
}


module.exports = Image;
