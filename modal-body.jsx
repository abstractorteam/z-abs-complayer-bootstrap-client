
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import React from 'react';


class ModalBody extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.children, nextProps.children);
  }
  
  render() {
    return (
      <div className="modal-body">
        {this.props.children}
      </div>
    );
  }
}


module.exports = ModalBody;
