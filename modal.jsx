
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import GuidGenerator from 'z-abs-corelayer-cs/guid-generator';
import React from 'react';
import ReactDOM from 'react-dom';


class Modal extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.guid = GuidGenerator.create();
    this.guidHash = `#${this.guid}`;
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props, nextProps);
  }
  
  didMount() {
    $(this.guidHash).on('hidden.bs.modal', (e) => {
      this.props.onHide && this.props.onHide(e);
    });
  }
  
  didUpdate(prevProps, prevState) {
    if(!prevProps.show && this.props.show) {
      $(this.guidHash).modal('show');
    }
    else if(prevProps.show && !this.props.show) {
      $(this.guidHash).modal('hide');
    }
  }
  
  render() {
    return ReactDOM.createPortal(
      <div className="modal fade" id={this.guid} tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div className={`modal-dialog${this.props.className ? ' ' + this.props.className : ''}`} role="document">
          <div className="modal-content">
            {this.props.children}
          </div>
        </div>
      </div>,
      document.body
    );
  }
}


module.exports = Modal;
