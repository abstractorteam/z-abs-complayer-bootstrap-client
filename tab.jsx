
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import GuidGenerator from 'z-abs-corelayer-cs/guid-generator';
import React from 'react';


class Tab extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.id = props.id ? props.id : GuidGenerator.create();
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompareObjectValues(this.props, nextProps);
  }
  
  render() {
    return (
      <React.Fragment key={this.id}>
        {this.props.children}
      </React.Fragment>
    );
  }
}


module.exports = Tab;
