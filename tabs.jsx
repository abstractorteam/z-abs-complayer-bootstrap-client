
'use strict';

import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import React from 'react';


class Tabs extends ReactComponentBase {
  constructor(props) {
    super(props, {
      isDragging: false,
      dragEventKey: ''
    });
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompareObjectValues(this.props, nextProps);
  }
  
  renderTabTitle(child, index) {
    const eventKey = undefined === child.props.eventKey ? index : child.props.eventKey;
    return (
      <a id={child.props.id} className="bootstrap_tab" href={child.props.tab} draggable={this.props.draggable}
        onClick={(evt) => {
          evt.stopPropagation();
          evt.preventDefault();
          if(this.props.onSelect) {
            this.props.onSelect(eventKey);
          }
        }}
        {...(this.props.draggable && {
          onDragStart:(evt) => {
            evt.dataTransfer.setData('text', eventKey);
          },
          onDragOver:(evt) => {
            evt.preventDefault();
          },
          onDrop:(evt) => {
            evt.preventDefault();
            evt.stopPropagation();
            const fromEventKey = evt.dataTransfer.getData('text');
            if(eventKey !== fromEventKey) {
              child.props.onMove && child.props.onMove(fromEventKey, eventKey);
            }
          }
        })}
      >
        {child.props.title}
      </a>
    );
  }
  
  renderTab(child, index, activeKey) {
    const cmpVal = undefined === child.props.eventKey ? index : child.props.eventKey;
    const active = (activeKey === cmpVal ? 'active' : '');
    return (
      <li role="presentation" key={index} className={active}>
        {this.renderTabTitle(child, index)}
      </li>
    );
  }
  
  renderTabs(activeKey) {
    const tabs = this.props.children.map((child, index) => {
      return this.renderTab(child, index, activeKey);
    });
    return (
      <ul className="nav nav-tabs">
        {tabs}
      </ul>
    );
  }
  
  renderContent(child, index, activeKey) {
    const cmpVal = undefined === child.props.eventKey ? index : child.props.eventKey;
    const active = (activeKey === cmpVal ? ' active' : '');
    return (
      <div key={index} className={`tab-pane bootstrap_same_size_as_parent${active}`} >
        {child}
      </div>
    );
  }
  
  renderContents(activeKey) {
    const contents = this.props.children.map((child, index) => {
      return this.renderContent(child, index, activeKey);
    });
    return (
      <div className="tab-content clearfix">
        {contents}
      </div>
    );
  }
  
  render() {
    const className = this.props.className ? ` ${this.props.className}` : '';
    return (
      <div id={this.props.id} className={`bootstrap_same_size_as_parent${className}`}>
        {this.renderTabs(this.props.activeKey)}
        {this.renderContents(this.props.activeKey)}
      </div>
    );
  }
}


module.exports = Tabs;
