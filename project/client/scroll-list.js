
'use strict';

import React from 'react';

  
class ScrollList {
  constructor(cbAutoScroll) {
    this.cbAutoScroll = cbAutoScroll;
    this.scrollDiv = null;
    this.scrollDivHeight = 0.0;
    this.scrollDivHeightParts = 0.0;
    this.listDiv = null;
    this.divNodeFirst = document.createElement('div');
    this.divNodeLast = document.createElement('div');
    this.divNodegroupTemplate = document.createElement('div');
    this.divNodegroupTemplate.classList.add('scroll_list_node_group');
    this.divNodeGroup = null;

    this.allNodes = [];
    this.currentIndex = 0;
    this.currentFirst = 0;
    this.currentLast = 0;
    this.render = false;
    
    this.scrollPosition = 0.0;
    this.scrollPositionNext = 0.0;
    this.scrollPositionPrevious = 0.0;
    this.isScrolling = false;
    
    this.autoScroll = true;
    this.zoom = 1.0;
    this.boundScroll = this._scroll.bind(this);
    this.boundResize = this._resize.bind(this);
  }
  
  init(scrollDiv, listDiv, autoScroll, zoom) {
    this.scrollDiv = scrollDiv;
    this.listDiv = listDiv;
    this.listDiv.appendChild(this.divNodeFirst);
    this.listDiv.appendChild(this.divNodeLast);
    this.autoScroll = autoScroll;
    this.zoom = zoom;
    this.scrollDivHeight = this.scrollDiv.clientHeight;
    this.scrollDivHeightParts = this.scrollDivHeight / ScrollList.NBR_OF_AUTO_SCROLL_PARTS;
    this.render = 0 !== this.scrollDivHeight;
    this._clear();
    
    this.scrollDiv.addEventListener('scroll', this.boundScroll, {capture:true,passive:false});
    window.addEventListener('resize', this.boundResize, true);
  }
  
  update(autoScroll, zoom) {
    const height = this.scrollDiv.clientHeight;
    const doUpdate = this.zoom !== zoom || this.scrollDivHeight !== height;
    const scrollUpdate = this.autoScroll !== autoScroll;
    this.autoScroll = autoScroll;
    if(doUpdate) {
      this.zoom = zoom;
      if(0 !== height) {
        this.scrollDivHeight = height;
        this.scrollDivHeightParts = this.scrollDivHeight / ScrollList.NBR_OF_AUTO_SCROLL_PARTS;
        this.render = 0 !== this.scrollDivHeight;
        this.recalculate();
      }
    }
    else if(scrollUpdate) {
      this._autoScroll();
    }
  }
  
  exit() {
    window.removeEventListener('resize', this.boundResize, true);
    this.scrollDiv.removeEventListener('scroll', this.boundScroll, {capture:true,passive:false});
  }
  
  _clear() {
    this.allNodes = [
      {
        startPos: 0,
        stopPos: 0,
        size: 0,
        sizeNormal: 0,
        items: [],
        qItems: [],
        divNodeGroup: this.divNodegroupTemplate.cloneNode(true)
      }
    ];
    this.currentIndex = 0;
    this.currentFirst = 0;
    this.currentLast = 0;
    
    this.scrollPosition = 0.0;
    this.scrollPositionNext = 0.0;
    this.scrollPositionPrevious = 0.0;
    this.isScrolling = false;
    
    // REMOVE NODES
    while(3 <= this.listDiv.childNodes.length) {
      this.listDiv.removeChild(this.divNodeFirst.nextSibling);
    }
    
    this.listDiv.firstChild.setAttribute('style', 'height:0px;');
    this.listDiv.lastChild.setAttribute('style', 'height:0px;');
  }
  
  _scroll(e) {
    if(!this.autoScroll) {
      this._setPosition(e.target.scrollTop);
    }
    else {
      if(e.target.scrollTop < this.scrollPositionPrevious) {
        this.autoScroll = false;
        this.cbAutoScroll(this.autoScroll);
      }
    }
  }
  
  _resize(e) {
    const height = this.scrollDiv.clientHeight;
    if(0 !== height) {
      this.scrollDivHeight = height;
    }
    this.scrollDivHeightParts = this.scrollDivHeight / ScrollList.NBR_OF_AUTO_SCROLL_PARTS;
    this.render = 0 !== this.scrollDivHeight;
    this.recalculate();
  }
  
  addItem(item) {
    const lastNode = this.allNodes[this.allNodes.length - 1];
    const size = this.zoom * item.height;
    lastNode.stopPos += size;
    lastNode.size += size;
    lastNode.sizeNormal += item.height;
    lastNode.items.push(item);
    lastNode.qItems.push(item);
    if(lastNode.sizeNormal >= this.scrollDivHeightParts) {
      const nextNode = {
        startPos: lastNode.stopPos,
        stopPos: lastNode.stopPos,
        size: 0,
        sizeNormal: 0,
        items: [],
        qItems: [],
        divNodeGroup: this.divNodegroupTemplate.cloneNode(true)
      };
      this.allNodes.push(nextNode);
    }
  }
  
  recalculate() {
    if(this.render) {
      for(let i = this.currentIndex; i < this.allNodes.length ; ++i) {
        const currentNode = this.allNodes[i];
        currentNode.qItems.forEach((item) => {
          currentNode.divNodeGroup.appendChild(item.event);
        });
        currentNode.qItems = [];
      }
      this.currentIndex = this.allNodes.length - 1;

      const previousSize = this.allNodes[this.allNodes.length - 1].stopPos;
      let startPos = 0;
      this.allNodes.forEach((node) => {
        let size = 0;
        node.startPos = startPos;
        node.items.forEach((item) => {
          size += this.zoom * item.height;
        });
        const newPos = startPos + size;
        node.stopPos = newPos;
        node.size = size;
        startPos = newPos;
      });
      // REMOVE NODES
      while(3 <= this.listDiv.childNodes.length) {
        this.listDiv.removeChild(this.divNodeFirst.nextSibling);
      }
      this.currentFirst = 0;
      this.currentLast = 0;
      this._autoScroll();
    }
  }
    
  _autoScroll() {
    if(this.render && this.autoScroll && 0 !== this.allNodes.length) {
      const scrollPosition = Math.max(this.allNodes[this.allNodes.length - 1].stopPos - this.scrollDivHeight, 0);
      this._setPosition(scrollPosition);
      this.scrollDiv.scrollTop = scrollPosition;
    }
    else {
      this._setPosition(this.scrollPosition);
    }
  }
  
  setAutoScroll(autoScroll) {
    this.autoScroll = autoScroll;
    this._autoScroll();
  }
  
  setZoom(zoom) {
    const doUpdate = this.zoom !== zoom;
    if(doUpdate) {
      this.zoom = zoom;
      this.recalculate();
    }
  }
  
  _setPosition(pos) {
    if(this.render) {

      this.scrollPositionPrevious = this.scrollPosition;
      this.scrollPosition = pos;
      let startIndex = 0;
      while(this.allNodes[startIndex].startPos < pos) {
        ++startIndex;
      }
      const visualParts = 2* Number.parseInt(ScrollList.NBR_OF_AUTO_SCROLL_PARTS / Math.min(this.zoom, 1.0));
      const extraParts = 2* Math.max(Number.parseInt(ScrollList.NBR_OF_AUTO_SCROLL_EXTRA_PARTS / Math.min(this.zoom, 1.0)), 1);
      const firstIndex = Math.max(0, startIndex - extraParts);
      const lastIndex = Math.min(this.allNodes.length - 1, startIndex + visualParts + extraParts);
      if(this.currentFirst < firstIndex || this.currentLast < lastIndex) {
        const rExistingAmount = this.listDiv.childNodes.length - 2;
        const removeAmount = firstIndex - this.currentFirst >= rExistingAmount ? rExistingAmount : firstIndex - this.currentFirst;
        // REMOVE nodes.
        for(let i = 0; i < removeAmount; ++i) {
          this.listDiv.removeChild(this.divNodeFirst.nextSibling);
        }
        const aExistingAmount = this.listDiv.childNodes.length - 2;
        const addAmount = lastIndex - firstIndex - aExistingAmount + 1;
        // ADD nodes.
        for(let i = 0; i < addAmount; ++i) {
          const node = this.allNodes[firstIndex + aExistingAmount + i];
          this.listDiv.insertBefore(node.divNodeGroup, this.divNodeLast);
        }
      }
      else if(this.currentFirst > firstIndex || this.currentLast > lastIndex) {
        const rExistingAmount = this.listDiv.childNodes.length - 2;
        const removeAmount = this.currentLast - lastIndex >= rExistingAmount ? rExistingAmount : this.currentLast - lastIndex;
        // REMOVE nodes.
        for(let i = 0; i < removeAmount; ++i) {
          this.listDiv.removeChild(this.divNodeLast.previousSibling);
        }
        const aExistingAmount = this.listDiv.childNodes.length - 2;
        const addAmount = lastIndex - firstIndex - aExistingAmount + 1;
        // ADD nodes.
        for(let i = 0; i < addAmount; ++i) {
          const node = this.allNodes[lastIndex - aExistingAmount - i];
          this.listDiv.insertBefore(node.divNodeGroup, this.divNodeFirst.nextSibling);
        }
      }
      this.currentFirst = firstIndex;
      this.currentLast = lastIndex;
      this.listDiv.firstChild.setAttribute('style', `height:${this.allNodes[firstIndex].startPos / this.zoom}px;`);
      this.listDiv.lastChild.setAttribute('style', `height:${(this.allNodes[this.allNodes.length - 1].stopPos - this.allNodes[lastIndex].stopPos) / this.zoom}px;`);
    }
  }
  
  renderRealtimeFrame(clear) {
    if(!clear) {
      for(let i = this.currentIndex; i < this.allNodes.length ; ++i) {
        const currentNode = this.allNodes[i];
        currentNode.qItems.forEach((item) => {
          currentNode.divNodeGroup.appendChild(item.event);
        });
        currentNode.qItems = [];
      }
      this.currentIndex = this.allNodes.length - 1;
      this._autoScroll();
    }
    else {
      this._clear();
    }
  }
}

ScrollList.NBR_OF_AUTO_SCROLL_PARTS = 6;
ScrollList.NBR_OF_AUTO_SCROLL_EXTRA_PARTS = 3;

module.exports = ScrollList;
